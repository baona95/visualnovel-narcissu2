<?php

if (php_sapi_name() !== 'cli')
{
    die;
}

$input = $argv[1];
$output = $argv[2];
$startLine = (int) $argv[3];
$endLine = (int) $argv[4];
$append = isset($argv[5]) && $argv[5] == 'a';

$content = file_get_contents($input);

$lineNo = 0;
$lines = preg_split('#\n|\r\n#', $content);

$replacedLines = [];

$patterns = array_keys(replacements());
$replacements = array_values(replacements());

$combineLineTrack = [];

foreach ($lines as $line)
{
    $lineNo++;
    if ($lineNo < $startLine)
    {
        continue;
    }

    $line = trim($line);
    $combineLineTrack[$lineNo] = !endsWith($line, '\\') && !endsWith($line, '^@^');
    $line = preg_replace($patterns, $replacements, $line);

    if ($line != '')
    {
        if ($combineLineTrack[$lineNo - 1] && !empty($replacedLines[$lineNo - 1]))
        {
            $replacedLines[$lineNo - 1] .= ' ' . ltrim($line, "'");
        }
        else
        {
            $replacedLines[$lineNo] = $line;
        }
    }

    if ($lineNo == $endLine)
    {
        break;
    }
}

$replacedContent = finalize(implode("',\n", $replacedLines)) . "',";

file_put_contents($output, $replacedContent, $append ? FILE_APPEND : 0);

function finalize(string $string): string
{
    // return $string;
    return str_replace(['^@^'], [''], $string);
}

function replacements(): array
{
    return [

        '#^mp3\s*\"(.*?)\".*#' => "'play music $1 noloop",
        '#^mp3loop\s*\"(.*?)\".*#' => "'play music $1 loop",

        '#^dwave\s*.*?,\"(.*?)\".*#' => "'play sound $1 noloop",
        '#^dwaveloop\s*.*?,\"(.*?)\".*#' => "'play sound $1 loop",

        '#^mp3stop$#' => "'stop music",
        '#^dwavestop.*$#' => "'stop sound",
        
        '#^bg\s*\"(.*?)\".*#' => "'scene $1",
        
        '#^br.*#' => "'...",
        '#^erasetextwindow.*#' => "'clear",
        
        '#^\^([^;]+)\\\\$#' => "'$1",
        '#^\^([^;]+)\^@\^$#' => "'$1",
        '#^\^([^;]+)$#' => "'$1",
        
        '#^!w(\d+)$#' => "'wait $1",
        '#^wait\s*(\d+)$#' => "'wait $1",
        '#^!d(\d+)$#' => "'wait $1",

        "#~i~(.*?)~i~#" => "<i>$1</i>",
        
        // clean up
        '#^setwindow.*#' => "",
        '#^bgmfadeout.*#' => "",
        '#^mp3fadeout.*#' => "",
        '#^;.*$#' => "",
        "#^[^'].*$#" => "",
        '#\\\\#' => "/",
        "#(?<!^)'#" => "\'",
    ];
}

function endsWith($string, $needle)
{
    return mb_substr($string, -mb_strlen($needle)) === $needle;
}